var express = require('express')
var mongoose = require('mongoose')
var bodyparse = require('body-parser')
var cors = require('cors')


var app = express()
const PORT = 2000;
var router = require('./router/router.js');

var dbpath = "mongodb://localhost:27017/products";

mongoose.connect(dbpath, (err,db)=>{
    if(err){
        console.log("DB Not Connected."+"Error:"+err)
    }
    else{
        console.log("DB Connected");
    }
});

// link middleware
app.use(bodyparse.json());
app.use(cors());
app.use('/app',router);


// app.get('/home', (req,res)=>{
//     console.log(req.url);
//     res.send("Welcome to my app");
// });

// app.get('/register' ,(req,res)=>{
//     console.log(req.url);
//     res.send("register Success");
// });

app.listen(PORT,()=>{
    console.log("Server Started at PORT:" +PORT);
})