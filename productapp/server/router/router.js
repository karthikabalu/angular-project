var express = require('express');
var router = express.Router();
var product = require('../model/productmodel.js');

router.post('/addproduct', (req,res,next)=>{
    let newproduct ={
        product_id: req.body.pid,
        product_name: req.body.name,
        product_price: req.body.price
    }
    product.insertMany(newproduct,(err, response)=>{
        if(err){
            res.json({"msg":"Data Not Inserted","error":err});
        }
        else{
            res.json({"msg":"Product added", "response":response});
        }
    });
});

router.get('/getproduct',(req,res,next)=>{
    product.find((err,dbdata)=>{
        if(err){
            res.json({"msg":"Data Not Available","Error":err});
        }
        else{
            res.send(dbdata);
                  // OR //
            // res.json({"DATA":dbdata});
        }
    });
});

router.delete('/delproduct/:id', (req,res,next)=>{
        // console.log(JSON.parse(req.params.id));
        // console.log(req.url);

    product.remove({_id:req.params.id},(err,response)=>{
        if(err){
            res.json({"msg":"Data Not Deleted"});
        }
        else{
            res.json({"msg":"Data Deleted"});
        }
    });
});

router.put('/editproduct/:id', (req,res,next)=>{
    let editproduct ={
        product_id: req.body.pid,
        product_name: req.body.name,
        product_price: req.body.price
    }

    product.update({_id:req.params.id},{$set:editproduct},(err,response)=>{
        if(err){
            res.json({"msg":"Data Not Updated"});
        }
        else{

            res.json({"msg":"Data Updated"});
        }
    });
});
// ------------------ TO GET single data ----------------
router.get('/getproduct/:id', (req,res,next)=>{
    product.find({_id:req.params.id}, (err,data)=>{
        if(err){
           res.json({"msg":"Data not available"});
        }
        else{
          res.json({"data":data});
        }
    });
});

module.exports = router;