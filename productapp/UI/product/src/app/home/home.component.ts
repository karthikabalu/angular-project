import { Component, OnInit } from '@angular/core';
import { ProductService } from '../product.service';
import { Product } from '../product';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

    products: Product[];
    proId : string;
    proName : string;
    proPrice : string;
    proObjid : string;

  constructor(private serObj: ProductService) { }

  ngOnInit() {
    this.display();
  }

  display(){
    this.serObj.showProduct().subscribe(
    (res)=>{
      this.products = res;
    },
    (err)=>{
      console.log("Error Occured");
    });
}

addProduct(newproduct){
  console.log(newproduct);

  this.serObj.createProduct(newproduct).subscribe(
    (res)=>{
      console.log(res);
      this.display();
    },
    (err)=>{
      console.log("Error Ocurred"+err);
    });
}

delProduct(objid){
  console.log(objid);
  this.serObj.deleteProduct(objid).subscribe(
    (res)=>{
      console.log(res);
      this.display();
    },
    (err)=>{
      console.log(err);
    });
}
edit(product){
    this.proObjid = product._id;
    this.proId = product.product_id;
    this.proName = product.product_name;
    this.proPrice = product.product_price;
}

update(product){
  console.log(product);
  console.log(this.proObjid);
  this.serObj.editProduct(this.proObjid, product).subscribe(
    (res)=>{
      console.log(res);
      this.display();
    },
    (err)=>{
      console.log(err);
    });
}

}
